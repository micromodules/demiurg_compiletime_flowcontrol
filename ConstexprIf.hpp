#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_flowcontrol__Version

template<bool T_Condition, typename T_RetType, const T_RetType& T_kRetIfTrue, const T_RetType& T_kRetIfFalse>
struct ConstexprConditionalRetExec;

template<typename T_RetType, const T_RetType& T_kRetIfTrue, const T_RetType& T_kRetIfFalse>
struct ConstexprConditionalRetExec<true, T_RetType, T_kRetIfTrue, T_kRetIfFalse> {
    static constexpr const T_RetType& _() { return T_kRetIfTrue; }
};

template<typename T_RetType, const T_RetType& T_kRetIfTrue, const T_RetType& T_kRetIfFalse>
struct ConstexprConditionalRetExec<false, T_RetType, T_kRetIfTrue, T_kRetIfFalse> {
    static constexpr const T_RetType& _() { return T_kRetIfFalse; }
};

#endif
