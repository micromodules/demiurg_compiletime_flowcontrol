#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_flowcontrol__Version

#include "demiurg_typescore/TypesPack.hpp"

namespace CombinationsDetails
{
    // ------------------------ Exec

    template<template<class ...> class T_Exec,
             class T_FilledPlaces,
             class T_ProcessingPlaceVariants,
             class ... T_LeftPlacesVariants>
    struct Exec;

    template<template<class ...> class T_Exec,
            class ... T_FilledPlacesTypes,
            class T_ProcessingPlaceVariantsTypesHead, class ... T_ProcessingPlaceVariantsTypesTail,
            class ... T_LeftPlacesVariantsHeadTypes, class ... T_LeftPlacesVariantsTail>
    struct Exec<T_Exec,
            TypesPack<T_FilledPlacesTypes ...>,
            TypesPack<T_ProcessingPlaceVariantsTypesHead, T_ProcessingPlaceVariantsTypesTail ...>,
            TypesPack<TypesPack<T_LeftPlacesVariantsHeadTypes ...>, T_LeftPlacesVariantsTail ...>>
    {
        static void _() {
            //Calling to deeper level
            Exec<T_Exec,
                    TypesPack<T_FilledPlacesTypes ..., T_ProcessingPlaceVariantsTypesHead>,
                    TypesPack<T_LeftPlacesVariantsHeadTypes ...>,
                    TypesPack<T_LeftPlacesVariantsTail ...>>::_();

            //Iterating in same level
            Exec<T_Exec,
                    TypesPack<T_FilledPlacesTypes ...>,
                    TypesPack<T_ProcessingPlaceVariantsTypesTail ...>,
                    TypesPack<TypesPack<T_LeftPlacesVariantsHeadTypes ...>, T_LeftPlacesVariantsTail ...>>::_();
        }
    };

    template<template<class ...> class T_Exec,
            class ... T_FilledPlacesTypes,
            class T_ProcessingPlaceVariantsTypesHead,
            class ... T_LeftPlacesVariantsHeadTypes, class ... T_LeftPlacesVariantsTail>
    struct Exec<T_Exec,
            TypesPack<T_FilledPlacesTypes ...>,
            TypesPack<T_ProcessingPlaceVariantsTypesHead>,
            TypesPack<TypesPack<T_LeftPlacesVariantsHeadTypes ...>, T_LeftPlacesVariantsTail ...>>
    {
        static void _() {
            //Calling to deeper level
            Exec<T_Exec,
                    TypesPack<T_FilledPlacesTypes ..., T_ProcessingPlaceVariantsTypesHead>,
                    TypesPack<T_LeftPlacesVariantsHeadTypes ...>,
                    TypesPack<T_LeftPlacesVariantsTail ...>>::_();
        }
    };

    template<template<class ...> class T_Exec,
            class ... T_FilledPlacesTypes,
            class T_ProcessingPlaceVariantsTypesHead, class ... T_ProcessingPlaceVariantsTypesTail>
    struct Exec<T_Exec, TypesPack<T_FilledPlacesTypes ...>,
            TypesPack<T_ProcessingPlaceVariantsTypesHead, T_ProcessingPlaceVariantsTypesTail ...>,
            TypesPack<>>
    {
        static void _() {
            //Calling of Exec
            T_Exec<T_FilledPlacesTypes ..., T_ProcessingPlaceVariantsTypesHead>::_();

            //Iterating in same level
            Exec<T_Exec,
                    TypesPack<T_FilledPlacesTypes ...>,
                    TypesPack<T_ProcessingPlaceVariantsTypesTail ...>,
                    TypesPack<>>::_();
        }
    };

    template<template<class ...> class T_Exec,
            class ... T_FilledPlacesTypes,
            class T_ProcessingPlaceVariantsTypesHead>
    struct Exec<T_Exec, TypesPack<T_FilledPlacesTypes ...>,
            TypesPack<T_ProcessingPlaceVariantsTypesHead>,
            TypesPack<>>
    {
        static void _() {
            //Calling of Exec
            T_Exec<T_FilledPlacesTypes ..., T_ProcessingPlaceVariantsTypesHead>::_();
        }
    };

    template<template<class ...> class T_Exec, class ... T_PlacesVariants>
    struct ExecCall;

    template<template<class ...> class T_Exec, class T_HeadVariant, class ... T_TailVariants>
    struct ExecCall<T_Exec, T_HeadVariant, T_TailVariants ...>
    {
        static void _() {
            Exec<T_Exec,
                    TypesPack<>,
                    T_HeadVariant,
                    TypesPack<T_TailVariants ...>>::_();
        }
    };

    // ------------------------ Gen

    template<template<class ...> class T_TypeToFill,
             class T_FilledPlaces,
             class T_ProcessingPlaceVariants,
             class ... T_LeftPlacesVariants>
    struct Gen;

    template<template<class ...> class T_TypeToFill,
            class ... T_FilledPlacesTypes,
            class T_ProcessingPlaceVariantsTypesHead, class ... T_ProcessingPlaceVariantsTypesTail,
            class ... T_LeftPlacesVariantsHeadTypes, class ... T_LeftPlacesVariantsTail>
    struct Gen<T_TypeToFill,
            TypesPack<T_FilledPlacesTypes ...>,
            TypesPack<T_ProcessingPlaceVariantsTypesHead, T_ProcessingPlaceVariantsTypesTail ...>,
            TypesPack<TypesPack<T_LeftPlacesVariantsHeadTypes ...>, T_LeftPlacesVariantsTail ...>>
    {
        using _ = typename ConcatTypes<
            /*Generate deeper level*/
            typename Gen<T_TypeToFill,
                TypesPack<T_FilledPlacesTypes ..., T_ProcessingPlaceVariantsTypesHead>,
                TypesPack<T_LeftPlacesVariantsHeadTypes ...>,
                TypesPack<T_LeftPlacesVariantsTail ...>>::_,
            /*Generate same level*/
            typename Gen<T_TypeToFill,
                TypesPack<T_FilledPlacesTypes ...>,
                TypesPack<T_ProcessingPlaceVariantsTypesTail ...>,
                TypesPack<TypesPack<T_LeftPlacesVariantsHeadTypes ...>, T_LeftPlacesVariantsTail ...>>::_
        >::_;
    };

    template<template<class ...> class T_TypeToFill,
            class ... T_FilledPlacesTypes,
            class T_ProcessingPlaceVariantsTypesHead,
            class ... T_LeftPlacesVariantsHeadTypes, class ... T_LeftPlacesVariantsTail>
    struct Gen<T_TypeToFill,
            TypesPack<T_FilledPlacesTypes ...>,
            TypesPack<T_ProcessingPlaceVariantsTypesHead>,
            TypesPack<TypesPack<T_LeftPlacesVariantsHeadTypes ...>, T_LeftPlacesVariantsTail ...>>
    {
        /*Generate deeper level*/
        using _ = typename Gen<T_TypeToFill,
            TypesPack<T_FilledPlacesTypes ..., T_ProcessingPlaceVariantsTypesHead>,
            TypesPack<T_LeftPlacesVariantsHeadTypes ...>,
            TypesPack<T_LeftPlacesVariantsTail ...>>::_;
    };

    template<template<class ...> class T_TypeToFill,
            class ... T_FilledPlacesTypes,
            class T_ProcessingPlaceVariantsTypesHead, class ... T_ProcessingPlaceVariantsTypesTail>
    struct Gen<T_TypeToFill, TypesPack<T_FilledPlacesTypes ...>,
            TypesPack<T_ProcessingPlaceVariantsTypesHead, T_ProcessingPlaceVariantsTypesTail ...>,
            TypesPack<>>
    {
        using _ = typename ConcatTypes<
            /*Generate filled type*/
            T_TypeToFill<T_FilledPlacesTypes ..., T_ProcessingPlaceVariantsTypesHead>,
            /*Generate same level*/
            typename Gen<T_TypeToFill,
                    TypesPack<T_FilledPlacesTypes ...>,
                    TypesPack<T_ProcessingPlaceVariantsTypesTail ...>,
                    TypesPack<>>::_
        >::_;
    };

    template<template<class ...> class T_TypeToFill,
            class ... T_FilledPlacesTypes,
            class T_ProcessingPlaceVariantsTypesHead>
    struct Gen<T_TypeToFill, TypesPack<T_FilledPlacesTypes ...>,
            TypesPack<T_ProcessingPlaceVariantsTypesHead>,
            TypesPack<>>
    {
        /*Generate filled type*/
        using _ = T_TypeToFill<T_FilledPlacesTypes ..., T_ProcessingPlaceVariantsTypesHead>;
    };
}

// =======================================================================================

template<template<class ...> class T_Exec, class T_PlacesVariantsHead, class ... T_PlacesVariantsTail>
static void execCombinations() {
    CombinationsDetails::Exec<T_Exec,
            TypesPack<>,
            T_PlacesVariantsHead,
            TypesPack<T_PlacesVariantsTail ...>>::_();
}

template<template<class ...> class T_Exec, class T_PlacesVariantsHead, class ... T_PlacesVariantsTail>
using GenCombinations = CombinationsDetails::Gen<T_Exec,
    TypesPack<>,
    T_PlacesVariantsHead,
    TypesPack<T_PlacesVariantsTail ...>>;

// =======================================================================================

//NB: We use here struct only bacause C++ is not support partial function specialization
template<typename TExecsPack>
struct ExecAll;

template<typename ... TExecs>
struct ExecAll<TypesPack<TExecs ...>>
{
    static void _() {
        using expand_type = int[];
        (void)expand_type{ (TExecs::_(), 0)... };
    }
};

#endif
