#include "../DModuleCore.hpp"
#include "demiurg_typescore/_Module.hpp"

#if DMACRO_CheckModuleVersion(demiurg_typescore, 1, 1)
#define MODULE__demiurg_compiletime_flowcontrol__Version 1
#endif
