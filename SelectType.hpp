#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_flowcontrol__Version

template<bool kT_SelectFirstType, typename T_FirstType, typename T_SecondType>
struct SelectType;

template<typename T_FirstType, typename T_SecondType>
struct SelectType<true, T_FirstType, T_SecondType> { using _ = T_FirstType; };

template<typename T_FirstType, typename T_SecondType>
struct SelectType<false, T_FirstType, T_SecondType> { using _ = T_SecondType; };

#endif
