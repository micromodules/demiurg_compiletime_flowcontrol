#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_flowcontrol__Version

namespace DRefDetailsNamespace
{
    template<const bool T_Consition, typename T_TypeIfTrue, typename T_TypeIfFalse>
    struct SelectType { };
    template<typename T_TypeIfTrue, typename T_TypeIfFalse>
    struct SelectType<true, T_TypeIfTrue, T_TypeIfFalse> { typedef T_TypeIfTrue type; };
    template<typename T_TypeIfTrue, typename T_TypeIfFalse>
    struct SelectType<false, T_TypeIfTrue, T_TypeIfFalse> { typedef T_TypeIfFalse type; };

    template<class T_Predicate, class T_TypeIfNotFound, class... T_Tail>
    struct GetTypeByPredicateImpl;

    template<class T_Predicate, class T_TypeIfNotFound, class T_Head, class... T_Tail>
    struct GetTypeByPredicateImpl<T_Predicate, T_TypeIfNotFound, T_Head, T_Tail ...>
    {
        typedef typename SelectType<T_Predicate::template result<T_Head>(),
            T_Head,
            typename GetTypeByPredicateImpl<T_Predicate, T_TypeIfNotFound, T_Tail ...>::type
        >::type type;
    };

    template<class T_Predicate, class T_TypeIfNotFound, class T_Head>
    struct GetTypeByPredicateImpl<T_Predicate, T_TypeIfNotFound, T_Head>
    {
        typedef typename SelectType<T_Predicate::template result<T_Head>(),
            T_Head,
            T_TypeIfNotFound
        >::type type;
    };

    template<class T_Predicate, class T_TypeIfNotFound>
    struct GetTypeByPredicateImpl<T_Predicate, T_TypeIfNotFound>
    {
        typedef T_TypeIfNotFound type;
    };
}

// ============================================

template<class T_Predicate, class T_TypeIfNotFound, typename ... T_Args>
using GetTypeByPredicate = typename DRefDetailsNamespace::GetTypeByPredicateImpl<T_Predicate, T_TypeIfNotFound, T_Args ...>::type;

#endif
